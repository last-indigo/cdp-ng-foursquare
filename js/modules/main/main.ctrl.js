
angular.module('fsqrApp')
	.controller('MainCtrl', function ($scope, fsqrAPIservice) {
		$scope.city = "Lviv";
		$scope.category = "";
		$scope.search = function () {
			$scope.places = fsqrAPIservice.places({
				near: $scope.city,
				query: $scope.category
			});
		};
	});