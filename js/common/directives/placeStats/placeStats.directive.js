angular.module('fsqrApp')
    .directive('placeStats', function () {
        return {
            restrict: 'E',
            templateUrl: 'js/common/directives/placeStats/placeStats.tpl.html'
        }
    });