angular.module('fsqrApp')
    .directive('placeCard', function () {
        return {
            restrict: 'E',
            templateUrl: 'js/common/directives/placeCard/placeCard.tpl.html'
        }
    });