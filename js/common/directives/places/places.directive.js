angular.module('fsqrApp')
    .directive('places', function () {
        return {
            restrict: 'E',
            templateUrl: 'js/common/directives/places/places.tpl.html',
            link: function(scope, element, attrs){

                scope.filterQuery = ""; // to avoid calling method on 'undefined'

                scope.selectPlace = function (index) {
                    scope.selectedPlace = scope.places[index];
                };

                scope.filterPlaces = function (item) { // runs for every item in ng-repeat
                    return item.name.toLowerCase().indexOf(scope.filterQuery.toLowerCase()) >=0;
                    // if false - filter will hide this card
                };

            }
        }
    });