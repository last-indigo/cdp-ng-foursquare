angular.module('fsqrApp')
    .directive('searchForm', function () {
        return {
            restrict: 'E',
            templateUrl: 'js/common/directives/searchForm/searchForm.tpl.html'
        }
    });